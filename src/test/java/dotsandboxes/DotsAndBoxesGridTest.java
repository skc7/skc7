package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static
     * fields.
     * This field is a logger. Loggers are like a more advanced println, for writing
     * messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does
     * something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    // Create an instance of the game class for testing
    private DotsAndBoxesGrid game;

    @BeforeEach
    public void setUp() {
        // Initialize the game before each test
        game = new DotsAndBoxesGrid(4, 3, 2); // Initialize the game before each test
    }

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIX ME: You need to write tests for the two known bugs in the code.
    @Test
    public void squareBoxCompletion() {
        // Draw lines to create a square
        game.drawHorizontal(0, 0, 1); // Draw first line (bottom)
        game.drawVertical(0, 0, 1); // Draw second line (left)
        game.drawVertical(1, 0, 1); // Draw third line (right)
        game.drawHorizontal(0, 1, 1); // Draw fourth line (top)

        // Check if the square is completed
        assertTrue(game.boxComplete(0, 0), "Expected the square to be completed.");
    }

    @Test
    public void alreadyDrawnLineTestThrowsException() {
        // Draw a line once
        game.drawHorizontal(0, 0, 1); // Draw a line
        // Attempt to draw the same line again and expect an exception
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            game.drawHorizontal(0, 0, 1); // Attempt to draw the same line
        });

        // Optional: Check that the exception message is what we expect
        assertEquals("Line already drawn. Please draw next undrawn side of square", exception.getMessage());
    }
}
